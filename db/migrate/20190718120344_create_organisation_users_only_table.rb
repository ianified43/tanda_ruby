class CreateOrganisationUsersOnlyTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :users, :organisations do |t|
      t.index :user_id
      t.index :organisation_id
    end
  end
end
