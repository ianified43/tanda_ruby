class Organisation < ApplicationRecord
	
	has_many :shifts
	has_many :users, through: :shifts

	has_and_belongs_to_many :users
end

