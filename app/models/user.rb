class User < ApplicationRecord
	has_secure_password
	
	has_many :shifts
	has_many :organisations, through: :shifts

	has_and_belongs_to_many :organisations
	
end
