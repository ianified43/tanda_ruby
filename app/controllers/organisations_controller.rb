require 'date'

class OrganisationsController < ApplicationController
	def create
		org = Organisation.new(org_params)

		org.save
		redirect_to '/organisation'
	end

	def index
		orgs1 = Organisation.all

		user = User.find(session[:user_id])

		orgs2 = user.organisations.all

		@orgs = orgs1 - orgs2
	end

	def new

	end

	def newshift
		shift = Shift.new(shift_params)
		shift.save
		redirect_to '/home'
	end

	def show
	    @org = Organisation.find(params[:id])

	    @user = User.find(session[:user_id]) 

	    @shifts = Shift.where(organisation_id: @org.id)
	 #    @shifts = Array.new
	 #    holder = "["
	 #    x = 1
	 #    counter = @shifts2.count
	 #    @shifts2.each do |shift|

		# 	holder += '{ "user_name" +> "' + User.find(shift.user_id).name + '",'
		# 	holder += '"shift_date" => "' + shift.shift_date.strftime("%b %d,  %Y") + '",'
		# 	holder += '"start_time" => "' + shift.start_time.to_s + '",'
		# 	holder += '"finish_time" => "' + shift.finish_time.to_s + '",'
		# 	if x == counter
		# 		holder += '"break_length" => "' + shift.break_length.to_s + '"}'
		# 	else
		# 		holder += '"break_length" => "' + shift.break_length.to_s + '"},'
		# 	end
			
		# 	x+=1


		# end
		# holder += "]"
		# @shifts = holder
		

	end

	def join
		user = User.find(session[:user_id])
		org = Organisation.find(params[:search][:oid])
		user.organisations << org

		redirect_to '/organisation'
	end

	private
		def org_params
			params.require(:org).permit(:name, :hourly_rate)
		end
		def shift_params
			params.require(:shift).permit(:shift_date, :start_time, :finish_time, :break_length, :user_id, :organisation_id)
		end
end
