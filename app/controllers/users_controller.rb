class UsersController < ApplicationController
	
	def create
		user = User.new(user_params)

		if user.save
			session[:user_id] = user.id
			redirect_to '/home'
		else
			flash[:register_errors] = user.errors.full_messages
			redirect_to '/'
		end
	end

	def index
		if session[:user_id]        
	    	redirect_to '/home'
	    end
	end

	def logout
		session[:user_id] = nil         
	    redirect_to '/' 
	end

	private
		def user_params
			params.require(:user).permit(:name, :email, :password)
	end

end
