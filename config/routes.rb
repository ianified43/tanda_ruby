Rails.application.routes.draw do
	get '/' => 'users#index'
	post '/sessions' => 'sessions#create'
	post '/users' => 'users#create'
	get '/logout' => 'users#logout'
	post '/organisations' => 'organisations#create'
	get '/home' => 'home#index'
	get 'organisation/new' => 'organisations#new'
	post 'newshift' => 'organisations#newshift'
	post 'organisation/join' => 'organisations#join'
	get 'organisation' => 'organisations#index'
	get 'organisation/shifts/:id' => 'organisations#show'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
